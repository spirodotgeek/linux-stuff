#!/usr/bin/gawk -f
#
# SpamAssassin Statistics v2.1
# (c) 2010 by Spiro Harvey
# http://wtf.geek.nz
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Usage: $0 </var/log/spamassassin>
#
# HOW TO USE THIS SCRIPT:
#	To customise this script, you will need to add custom rules by
#	adding a bunch of rule_hit() lines to the rule section in the format:
#
#	rule_hit(type, name, rule)
#		type	= custom, bl (blacklist), wl (whitelist)
#		name	= a text description
#		rule	= the SA rule name to match
#
#	After that, add a matching print_hits() line in the display section.
#

BEGIN {
	if (ARGC < 2) {
		print "I need a file to process. Usually /var/log/spamassassin"
		x = 1
		exit 1
	}
	days	= ARGC - 1
	del	= 0
	sbx	= 0
	rej	= 0
	spam	= 0
}

/result: / {

        score	= $9
	trigger	= $11
	if ($8 == "Y") spam=1

	scantime($12)
	st = f[1]			# Message Scan Time
	sz = f[2]			# Message Size (bytes)

	# 
	# RULE SECTION
	#
	rule_hit("custom", "MedShop", "KNL_MEDSHOP")
	rule_hit("custom", "MIME/JPG", "KNL_MIME_JPG")
	rule_hit("custom", "ZIP file", "KNL_ZIP_FILE")
	rule_hit("custom", "Bachelor Degrees", "KNL_BACHEELOR")
	rule_hit("custom", "Tech Support Phishing", "KNL_TECHSUPPORT")
	rule_hit("custom", "Paypal Scam Phishing", "KNL_PAYPAL_SCAM")
	rule_hit("custom", "ANZ Phishing Scam", "KNL_ANZ_SCAM")

	rule_hit("bl", "Spamhaus SBL", "RCVD_IN_SBL")
	rule_hit("bl", "Spamhaus PBL", "RCVD_IN_PBL")
	rule_hit("bl", "Spamhaus XBL", "RCVD_IN_XBL")
	rule_hit("bl", "Spamhaus DBL", "URIBL_DBL_SPAM")
	rule_hit("bl", "SBL URI", "URIBL_SBL")
	rule_hit("bl", "NJABL", "RCVD_IN_NJABL")
	rule_hit("bl", "SORBS", "RCVD_IN_SORBS")
	rule_hit("bl", "Spamcop", "RCVD_IN_BL_SPAMCOP")
	rule_hit("bl", "SURBL URI", "URIBL_.._SURBL")
	rule_hit("bl", "RFC Ignorant", "DNS_FROM_RFC")

	rule_hit("wl", "IADB", "RCVD_IN_IADB")

	
	if(spam) {
		if (score < 10) {
			sbx++
			stst += st		# Spamboxed Total Scan Time
			stsz += sz		# Spamboxed Total Size
		}
		if (score >= 10) {
			rej++
			rtst += st		# Rejected Total Scan Time
			rtsz += sz		# Rejected Total Size
		}
		spam=0
	} else {
		del += 1 
		dtst += st			# Delivered Total Scan Time
		dtsz += sz			# Delivered Total Size
	}
}


END {
	if (x == 1) exit 1
	tot = sbx + rej + del
	spam = sbx + rej
	b = 0; c = 0; w = 0		# clear list counters

	printf "\n[7m TOTALS                                              [0m\n"
	printf "                              AvgTm  AvgThruput\n"
	printf "                # Msgs   %/Total (sec) (bytes/sec)\n"
	printf "             ~~~~~~~~~ ~~~~~~~~~ ~~~~~ ~~~~~~~~~~~\n"
	printf "  Delivered  %9d (%6.2f%) %5.2f %11.2f\n",		   \
			del, (del / tot) * 100, dtst / del, dtsz / dtst
	printf "  Spamboxed  %9d (%6.2f%) %5.2f %11.2f\n",		   \
			sbx, (sbx / tot) * 100, stst / sbx, stsz / stst
	printf "  Rejected   %9d (%6.2f%) %5.2f %11.2f\n\n",		   \
			rej, (rej / tot) * 100, rtst / rej, rtsz / rtst
	printf "  Total      %9d messages processed\n", tot
	printf "\n  (%d/day; %.2f/hr; %.2f/min; %.2f/sec)\n",              \
				tot / days, (tot / days) / 24,             \
				(tot / days) / 1440, (tot / days) / 86400

	#
	# DISPLAY SECTION:
	# Output rule hit results
	#
	if (num_hits("bl") > 0) {
		print_header("bl")
		print_hits("Spamhaus SBL")
		print_hits("Spamhaus PBL")
		print_hits("Spamhaus XBL")
		print_hits("Spamhaus DBL")
		print_hits("SBL URI")
		print_hits("NJABL")
		print_hits("SORBS")
		print_hits("Spamcop")
		print_hits("SURBL URI")
		print_hits("RFC Ignorant")
	}

	if (num_hits("wl") > 0) {
		print_header("wl")
		print_hits("IADB")
	}

	if (num_hits("custom") > 0) {
		print_header("custom")
		print_hits("MIME/JPG")
		print_hits("MedShop")
		print_hits("ZIP file")
	}

	printf "\n"
}

#
# Calculate the number of hits a rule type triggered
#
function num_hits(rt) {
	j = 0
	for (i in hit_type)
		if (hit_type[i] == rt) j++
	return j
}

#
# Extract the time it took to process the message
#
function scantime(data) {
	split(data, f, ",")
	sub("scantime=", "", f[1])
	sub("size=", "", f[2])
}

#
# Check if a rule is triggered
#
function rule_hit(type, name, rule) {
	if (trigger ~ rule) {
		hit_type[idx] = type
		hit_name[idx] = name
		hit_score[idx] = score
		idx++
	}
}

#
# Display the section headers in the output
#
function print_header(ht) {
	if (ht == "bl") 	header = "Blacklist"
	if (ht == "custom")	header = "Custom Rule"
	if (ht == "wl")		header = "Whitelist"

	printf "\n[7m"
	printf " %s HITS ", toupper(header)
	for (x = 0; x < (47 - length(header)); x++)
		printf " "
	printf "[0m\n"
	printf "  %-13s   Msgs   %/Total ", header
	if (ht == "bl") 
		printf "  %/Spam "
	printf "Avg Score\n"
	printf "  ~~~~~~~~~~~~~ ~~~~~~ ~~~~~~~~~ "
	if (ht == "bl") 
		printf "~~~~~~~~ "
	printf "~~~~~~~~~\n"
}

#
# Display statistics for given rule name if it was triggered
#
function print_hits (rname) {
	s = 0
	h = 0
	for (i in hit_type) {
		if (hit_name[i] == rname) {
			s += hit_score[i]
			h++
			ht = hit_type[i]
		}
	}
	if (h > 0) {
		printf "  %-13s %6d (%6.2f%) ", rname, h, (h / tot) * 100
		if (ht == "bl")
			printf "(%6.2f%) ",  (h / spam) * 100
		printf "%8.2f\n", s / h
	}
}
			

